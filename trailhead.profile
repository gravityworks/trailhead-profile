<?php

/**
 * @file
 * Enables modules and site configuration for a sparrow site installation.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\trailhead\Installer\Form\ModuleConfigureForm;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function trailhead_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['site_information']['site_name']['#default_value'] = 'Trailhead';
  $form['admin_account']['account']['name']['#default_value'] = 'GravityWorks';
}

/**
 * Implements hook_install_tasks().
 */
function trailhead_install_tasks(&$install_state) {
  return [
    '\Drupal\trailhead_site_settings\Form\TrailheadSiteSettingsForm' => [
      'display_name' => t('Site settings'),
      'display' => TRUE,
      'type' => 'form',
    ],
  ];
}

/**
 * Implements hook_install_tasks_alter().
 *
 * Unfortunately we have to alter the verify requirements.
 * This is because of https://www.drupal.org/node/1253774. The dependencies of
 * dependencies are not tested. So adding requirements to our install profile
 * hook_requirements will not work :(. Also take a look at install.inc function
 * drupal_check_profile() it just checks for all the dependencies of our
 * install profile from the info file. And no actually hook_requirements in
 * there.
 */
// function trailhead_install_tasks_alter(&$tasks, $install_state) {
//   // Allow the user to select optional modules and have Drupal install those for
//   // us. To make this work we have to position our optional form right before
//   // install_profile_modules.
//   $task_keys = array_keys($tasks);
//   $insert_before = array_search('install_profile_modules', $task_keys, TRUE);
//   $tasks = array_slice($tasks, 0, $insert_before - 1, TRUE) +
//     [
//       'trailhead_module_configure_form' => [
//         'display_name' => t('Select optional modules'),
//         'type' => 'form',
//         'function' => ModuleConfigureForm::class,
//       ],
//     ] +
//     array_slice($tasks, $insert_before - 1, NULL, TRUE);
// }
