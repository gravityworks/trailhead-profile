<?php

namespace Drupal\trailhead\Exception;

/**
 * An exception thrown by the installer for invalid Trailhead Feature data.
 */
class TrailheadFeatureDataException extends \RuntimeException {
}
